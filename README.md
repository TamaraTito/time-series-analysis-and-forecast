# Goal of the Project:
The objective of this project is to forecast Walmart sales over the upcoming 8-week period.

## Built with :
R 4.2.2
R studios 2022.12.0

## Packages used:
ggplot2
forecast

## Data Samples size:
sales file : 12.2 MB
features file: 578 KB
